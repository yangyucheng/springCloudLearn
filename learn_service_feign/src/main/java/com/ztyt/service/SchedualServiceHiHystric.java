/**
 * @Project：
 * @Title：SchedualServiceHiHystric.java
 * @Description：
 * @Package com.ztyt.service
 * @author：yangyucheng
 * @date：2019/5/13
 * @Copyright: 武汉中天云通数据科技有限责任公司  All rights reserved.
 * @version V1.0
 */

package com.ztyt.service;

import org.springframework.stereotype.Component;

@Component
public class SchedualServiceHiHystric implements SchedualServiceHi {

    @Override
    public String sayHiFromClientOne( String name ) {
        return "sorry "+name;
    }

}
