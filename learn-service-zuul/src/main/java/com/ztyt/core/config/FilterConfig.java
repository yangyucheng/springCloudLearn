/**
 * @Project：
 * @Title：FilterConfig.java
 * @Description：
 * @Package com.ztyt.core.config
 * @author：yangyucheng
 * @date：2019/5/13
 * @Copyright: 武汉中天云通数据科技有限责任公司  All rights reserved.
 * @version V1.0
 */

package com.ztyt.core.config;

import com.ztyt.core.filter.MyFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FilterConfig {

    @Bean
    public MyFilter myFilter(){
        return new MyFilter();
    }

}
