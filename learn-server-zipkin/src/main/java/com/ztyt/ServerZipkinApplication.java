/**
 * @Project：
 * @Title：ServerZipkinApplication.java
 * @Description：
 * @Package com.ztyt
 * @author：yangyucheng
 * @date：2019/5/13
 * @Copyright: 武汉中天云通数据科技有限责任公司  All rights reserved.
 * @version V1.0
 */

package com.ztyt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import zipkin.server.EnableZipkinServer;

@SpringBootApplication
@EnableZipkinServer
public class ServerZipkinApplication {

    public static void main(String[] args) {
        SpringApplication.run(ServerZipkinApplication.class, args);
    }

}



